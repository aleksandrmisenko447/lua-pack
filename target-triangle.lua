-- config
local MIN_COLOR = { h = 0, s = 1, v = 1 }
local MAX_COLOR = { h = 0.3, s = 1, v = 1 }
--
local abs, floor = math.abs, math.floor
local memory = require("memory")
do
   local bytes = "90909090909090909090909090C744240E00000000909090909090909090909090909090C744240F0000000090B300"
   memory.hex2bin(bytes, 0x60BB41, bytes:len() / 2)
end

local function lim(value)
   if value < 0 then 
      return 1 + value 
   end
   if value >= 1 then 
      return value - 1 
   end
   return value
end

local function lim_hp(v)
   if v < 0 then 
      return 0
   end
   if v >= 100 then 
      return 100
   end
   return v
end

local function switch(key)
   return function(tab)
      local current = tab[key] or tab.default
      if type(current) == 'function' then current() end
   end
end

local function hsv2rgb(h, s, v)
   local i = floor(h * 6)
   local f = h * 6 - i;
   local p = v * (1 - s)
   local q = v * (1 - f * s)
   local t = v * (1 - (1 - f) * s)
   
   local res = {}
   switch(i % 6) {
      [0] = function() res = { r = v, g = t, b = p } end,
      [1] = function() res = { r = q, g = v, b = p } end,
      [2] = function() res = { r = p, g = v, b = t } end,
      [3] = function() res = { r = p, g = q, b = v } end,
      [4] = function() res = { r = t, g = p, b = v } end,
      [5] = function() res = { r = v, g = p, b = q } end
   }
   res.r, res.g, res.b = res.r * 0xFF, res.g * 0xFF, res.b * 0xFF
   return res
end

local function scalar_lerp(min, max, k) 
   return min + k * (max - min)
end

local function color_lerp(c_min, c_max, k)
   local h, s, v = 0, 0, 0
   local dh = abs(c_max.h - c_min.h)
   if dh <= 0.5 then -- always use fast path
      h = scalar_lerp(c_min.h, c_max.h, k)
   else
      h = scalar_lerp(c_min.h, c_min.h + 1 - dh, -k)
   end
   h = lim(h)

	s = scalar_lerp(c_min.s, c_max.s, k)
	v = scalar_lerp(c_min.v, c_max.v, k)
	
   return hsv2rgb(h, s, v)
end

local function set_color(col)
   memory.setint8(0x60BB52, col.r, false)
   memory.setint8(0x60BB69, col.g, false)
   memory.setint8(0x60BB6F, col.b, false)
end

function main()
   repeat wait(0) until isSampAvailable()

   while true do wait(0)
      if not (sampIsChatInputActive() or isSampfuncsConsoleActive() or isCharInAnyCar(PLAYER_PED)) then
         local is_targeting, target = getCharPlayerIsTargeting(PLAYER_HANDLE)
         if is_targeting then
            local alive, id = sampGetPlayerIdByCharHandle(target)
            if alive then
               local k = lim_hp(sampGetPlayerHealth(id)) / 100
               set_color(color_lerp(MIN_COLOR, MAX_COLOR, k))
            end
         end
      end
   end
end
