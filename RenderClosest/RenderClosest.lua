script_name("RenderClosest")
script_author("THERION")
script_description("Renders the closest player if there is one")
script_version(2.0)
script_dependencies("imGui")

local BUFFER_SIZE = 64
local MULTILINE_BUFFER_SIZE = 4096
local CMD = "rndr"

-- prints message to SAMP Chat
local function log(msg)
   local format = "{FB4343}[%s]{FFFFFF}: %s{FFFFFF}."
   sampAddChatMessage(string.format(format, thisScript().name, msg), -1)
end

-- prints out if user does not have some of the needed files
local function catch_exception(status, path)
   local msg = string.format('File not found: "%s". Shutting down..', path)
   assert(status, msg)
end

do -- loading libraries
   local list = {
      memory     = "memory",
      -- ImGui
      clr_works  = "color_works",
      imgui      = "imgui",
      inicfg     = "inicfg",
      encoding   = "encoding",
      style      = string.format("config.%s.style", thisScript().name),
   }

   local result = nil
   for var, path in pairs(list) do
      result, _G[var] = pcall(require, path)
      catch_exception(result, path)
   end
   
   style.apply_style()
end

-- config container
ini = {}

encoding.default = "CP1251"
local u8 = encoding.UTF8
-- imgui window state
local win_state = imgui.ImBool(false)
-- team buffer
local team_buffer = imgui.ImBuffer(MULTILINE_BUFFER_SIZE)
-- font pointer
local font = nil
-- name-tag draw distance ^ 2
local nt_drawdist = 0
-- screen resolution        
local res_x, res_y = getScreenResolution()

local lang = { -- short | long name
   {"eng", "English"},  
   {"ua", u8:decode("Українська")} 
}

local txt = nil -- translated sentences container

-- check if element is in array
local function is_in_array(array, value)
   for _, element in ipairs(array) do
      if value == element then
         return true
      end
   end
   return false
end

-- check if player wears mask
local function check_hitman(player_id)
   local struct_ptr = sampGetPlayerStructPtr(player_id)
   local value = readMemory(struct_ptr, 4, true)
   return getStructElement(value, 179, 2, false) == 0
end

-- get distance squared
local function dist_sqrd(x1, y1, z1, x2, y2, z2)
   return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2) * (z1 - z2)
end

-- multiline text to string table
local function parse(s)
   local result = {}
   for chunk in string.gmatch(s, "[^\n]+") do
      result[#result + 1] = chunk
   end
   return result
end

-- limit number input
local function limit_input(min, max, var)
   if var < min then var = min end
   if max < var then	var = max end
   return var
end


-- find closest player if there is one
local function find_closest_id()
   local cam_x, cam_y, cam_z = getActiveCameraCoordinates()
   local ped_pool = getAllChars()
   local closest = nil
   local min_dist = 9999
   for _, ped in ipairs(ped_pool) do
      local result, id = sampGetPlayerIdByCharHandle(ped)
      local valid = result and ped ~= PLAYER_PED and not check_hitman(id)
      if valid and not is_in_array(ini.team, sampGetPlayerNickname(id)) then
         local ped_x, ped_y, ped_z = getCharCoordinates(ped)
         local current_dist = dist_sqrd(cam_x, cam_y, cam_z, ped_x, ped_y, ped_z)
         if nt_drawdist > current_dist and min_dist > current_dist then
            min_dist = current_dist
            closest = id
         end
      end
   end

   return closest 
end

-- update interface language
local function update_lang()
   local result = nil
   local file_name = lang[ini.settings.i_lang.v][1]
   
   local pattern = "config.%s.%s"
   local path = string.format(pattern, thisScript().name, file_name)
   result, txt = pcall(require, path)
   catch_exception(result, path)
end

function main()
   while not isSampAvailable() do wait(0) end

   --[[ not sure if this isn't useful anywhere else
   do
      local GALAXY_IP = {"176.32.39.200", "176.32.39.199", "176.32.39.198"}
      local ip, _ = sampGetCurrentServerAddress()
      if not is_in_array(GALAXY_IP, ip) then
         thisScript():unload()
      end
   end
   ]]

   do -- loading configuration
      local function load_cfg(table_name, default, settings)
         local cfg_path = "moonloader\\config\\" .. thisScript().name

         local pattern = "%s\\%s.ini"
         local default_path = string.format(pattern, cfg_path, default)
         if not doesFileExist(default_path) then
            catch_exception(false, default_path)
         end
         local settings_path = string.format(pattern, cfg_path, settings)

         local short_path = string.format(pattern, thisScript().name, settings)
         local short_def = string.format(pattern, thisScript().name, default)
         if not doesFileExist(settings_path) then
            _G[table_name] = inicfg.load(nil, short_def)
            inicfg.save(_G[table_name], short_path)
         else
            _G[table_name] = inicfg.load(nil, short_path)
         end
      end
      load_cfg("ini", "default", "settings")
   end



   do -- font init
      font = renderCreateFont(ini.settings.font, ini.settings.i_size, ini.settings.i_flag)
      renderFontDrawText(font, "000", 9999, 9999, -1)
   end

   print(string.format("settings: /%s", CMD))

   sampRegisterChatCommand(CMD, 
   function() 
      win_state.v = not win_state.v 
   end)
   
   imgui.to("team")

   update_lang()

   do -- getting nametag draw distance from memory
      local s_ptr = sampGetServerSettingsPtr()
      nt_drawdist = memory.getfloat(s_ptr + 39)
      nt_drawdist = nt_drawdist * nt_drawdist -- for easier calculations
   end

   while true do wait(0) 
      imgui.Process = win_state.v
      if ini.settings.enabled.v then
         local str = find_closest_id()
         local word = u8:decode(ini.settings.word.v)
         if not str then
            local not_found = u8:decode(ini.settings.not_found.v)
            str = string.format("%s %s", word, not_found)
         else
            if ini.settings.klist_color.v then
               local kl_color = clr_works.argb_to_rgb(sampGetPlayerColor(str))
               str = string.format("%s {%06X}%s[%d]", word, kl_color, sampGetPlayerNickname(str), str)
            else
               str = string.format("%s %s[%d]", word, sampGetPlayerNickname(str), str)
            end
         end

         local x = ini.settings.i_pos_x.v
         local align = ini.settings.i_alignment.v
         
         if align == 1 then
            --
         end
         if align == 2 then 
            x = x - renderGetFontDrawTextLength(font, str) / 2 
         end
         if align == 3 then 
            x = x - renderGetFontDrawTextLength(font, str) 
         end
         local clr = clr_works.imgui_to_argb(ini.settings.color_font)
         renderFontDrawText(font, str, x, ini.settings.i_pos_y.v, clr)
      end
   end
end

-- Convert data to imGui formats
imgui.to = function(ignored_section)
   for section_name, _ in pairs(ini) do
      if section_name ~= ignored_section then
         local settings = ini[section_name]
         for key, value in pairs(settings) do
            local data_type = type(settings[key])
            if data_type == "boolean" then 
               settings[key] = imgui.ImBool(value)
            end
            if data_type == "number" then
               if key:find("i_") then 
                  settings[key] = imgui.ImInt(value) 
               end
               if key:find("f_") then 
                  settings[key] = imgui.ImFloat(value) 
               end
               if key:find("color_") then
                  settings[key] = clr_works.argb_to_imgui(value)
               end
            end
            if data_type == "string" then
               local temp_str = value
               settings[key] = imgui.ImBuffer(BUFFER_SIZE)
               settings[key].v = u8(temp_str)
            end
         end
         ini[section_name] = settings
      end
   end
   -- and fill team_buffer
   team_buffer.v = u8(table.concat(ini.team, "\n"))
end

-- Get original data from imGui formats
imgui.from = function(ignored_section)
   for section_name, _ in pairs(ini) do
      if section_name ~= ignored_section then 
         local settings = ini[section_name]
         for key, value in pairs(settings) do
            local data_type = type(settings[key].v)
            if data_type == "number" or data_type == "boolean" then
               settings[key] = value.v
            end
            if data_type == "string" then
               settings[key] = u8:decode(value.v) 
            end
            if data_type == "userdata" then
               settings[key] = clr_works.imgui_to_argb(value)
            end
         end
      end
   end
end

function imgui.OnDrawFrame()
   if win_state.v then
      imgui.SetNextWindowPos(imgui.ImVec2(300, res_y / 5), imgui.Cond.FirstUseEver)
      imgui.Begin(thisScript().name, win_state, style.win_flags)
      imgui.Text(txt.LANG_SELECT_TEXT)

      do -- language selector
         for idx, data in ipairs(lang) do
            imgui.SameLine()
            if imgui.RadioButton(u8(data[2]), ini.settings.i_lang, idx) then
               update_lang()
            end

         end
      end

      imgui.Checkbox(txt.ON_OFF_TEXT, ini.settings.enabled) -- on/off switch
      
      if ini.settings.enabled.v then

         imgui.Checkbox(txt.CHEKBOX_TEXT_KLIST, ini.settings.klist_color) 
         -- check if k-list colour is displayed

         do  -- position selector
            imgui.PushItemWidth(148)
            imgui.InputInt("##i_pos_x input", ini.settings.i_pos_x, 0)
            imgui.SameLine()
            imgui.InputInt("##i_pos_y input", ini.settings.i_pos_y, 0)
            imgui.PopItemWidth()

            if imgui.Button(txt.CHOOSEPOSBUTTON_TEXT, imgui.ImVec2(300, 20)) then
               lua_thread.create(
               function()
                  win_state.v = false
                  while true do wait(0)
                     sampSetCursorMode(3)
                     ini.settings.i_pos_x.v, ini.settings.i_pos_y.v = getCursorPos()
                     if isKeyJustPressed(0x01) then
                        sampSetCursorMode(0)
                        win_state.v = true
                        return
                     end
                  end
               end)
            end
         end

         do -- font style settings
            imgui.PushItemWidth(100)
            imgui.Text(txt.FONT_SETTINGS_TEXT)
            imgui.SameLine(160)
            imgui.Text(txt.ALIGNMENT_TEXT)

            local function font_name()
               if imgui.InputText(txt.FONT_NAME_TEXT, ini.settings.font) then
                  font = renderCreateFont(u8:decode(ini.settings.font.v), ini.settings.i_size.v, ini.settings.i_flag.v)
               end
            end
            local function font_size()
               if imgui.InputInt(txt.FONT_SIZE_TEXT, ini.settings.i_size) then
                  ini.settings.i_size.v = limit_input(1, 100, ini.settings.i_size.v)
                  font = renderCreateFont(ini.settings.font.v, ini.settings.i_size.v, ini.settings.i_flag.v)
               end
            end
            local function font_flag()
               if imgui.InputInt(txt.FONT_FLAG_TEXT, ini.settings.i_flag) then
                  ini.settings.i_flag.v = limit_input(0, 13, ini.settings.i_flag.v)
                  font = renderCreateFont(ini.settings.font.v, ini.settings.i_size.v, ini.settings.i_flag.v)
               end
            end

            local align_types = {
               {txt.ALIGN_LEFT_TEXT, font_name},
               {txt.ALIGN_MIDDLE_TEXT, font_size},
               {txt.ALIGN_RIGHT_TEXT, font_flag},
            }
            for idx, data in ipairs(align_types) do
               data[2]()
               imgui.SameLine(160)
               imgui.RadioButton(data[1], ini.settings.i_alignment, idx)
            end
            imgui.PopItemWidth()
            imgui.PushItemWidth(250)
            imgui.ColorEdit4(txt.FONT_COLOR_TEXT, ini.settings.color_font)
            imgui.PopItemWidth()
         end



         do -- words to be displayed
            imgui.PushItemWidth(140)
            imgui.Text(txt.CURRENT_WORD_TEXT)
            imgui.SameLine(160)
            imgui.Text(txt.NOTFOUND_WORD_TEXT)
            imgui.InputText("##word", ini.settings.word)
            imgui.SameLine(160)
            imgui.InputText("##not_found", ini.settings.not_found)
            imgui.PopItemWidth()
         end

         do -- the exception input
            imgui.Text(txt.EXCEPTION_TEXT)
            if imgui.InputTextMultiline("##teambuffer", team_buffer, imgui.ImVec2(300, 200)) then
               ini.team = parse(u8:decode(team_buffer.v))
            end
         end
      end

      do -- save and restore default buttons
         local bottom_btn_size = imgui.ImVec2(147, 20)
         if imgui.Button(txt.SAVEBUTTON_TEXT, bottom_btn_size) then
            imgui.from("team")
            inicfg.save(ini, thisScript().name .. "\\settings.ini")
            imgui.to("team")
         end

         imgui.SameLine()

         if imgui.Button(txt.RESETBUTTON_TEXT, bottom_btn_size) then
            ini = inicfg.load(nil, thisScript().name .. "\\default.ini") 
            imgui.to("team")
         end
      end

      imgui.End()
   end
end
