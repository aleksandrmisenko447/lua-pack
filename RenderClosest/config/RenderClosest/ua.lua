-- encoding = UTF-8
local module = {
   LANG_SELECT_TEXT     = "Мова:",
   ON_OFF_TEXT          = "Виводити на екран найближчого гравця",
   SAVEBUTTON_TEXT      = "Зберегти",
   RESETBUTTON_TEXT     = "До початкових",
   CHOOSEPOSBUTTON_TEXT = "Визначити позицію",
   ALIGNMENT_TEXT       = "Виключка:",
   ALIGN_LEFT_TEXT      = "По лівому краю",
   ALIGN_MIDDLE_TEXT    = "По центру",
   ALIGN_RIGHT_TEXT     = "По правому краю",
   CHEKBOX_TEXT_KLIST   = "Використовувати колір із переліку вбивст",
   CURRENT_WORD_TEXT    = "Завжди додавати фразу:",
   NOTFOUND_WORD_TEXT   = "Нікого нема поблизу:",
   FONT_SETTINGS_TEXT   = "Шрифт: ",
   FONT_NAME_TEXT       = "Назва",
   FONT_SIZE_TEXT       = "Розмір",
   FONT_FLAG_TEXT       = "Стиль",
   FONT_COLOR_TEXT      = "Колір",
   EXCEPTION_TEXT       = "Вийняткові гравці:",
}

return module