script_name("Crosshair-dot")
script_author("THERION")

-- settings
local size = {x = 4, y = 4}
local corners = 12
local color = 0xFFFFFFFF
-- settings

local ffi = require("ffi")

local crosshair = {}

function main()
   repeat wait(0) until isSampAvailable()

   do
      local vec_t = "float[3]"
      local out, tmp = ffi.new(vec_t), ffi.new(vec_t)

      local the_camera = ffi.cast('void*', 0xB6F028)
      local compute_3d_person_mouse_target = ffi.cast('void (__thiscall*)(void*, float, float, float, float, float*, float*)', 0x514970)
      
      compute_3d_person_mouse_target(the_camera, 15.0, tmp[0], tmp[1], tmp[2], tmp, out)
      crosshair.x, crosshair.y =  convert3DCoordsToScreen(out[0], out[1], out[2])
   end

   while true do wait(0)
      local cam_mode = readMemory(0xB6F1A8, 1, false)
      if cam_mode ~= 7 and cam_mode ~= 8 and cam_mode ~= 51 then
         renderDrawPolygon(crosshair.x, crosshair.y, size.x, size.y, corners, 0, color)
      end
   end
end

