script_name("ML-reload")
script_author("THERION")
script_properties("work-in-pause")

require("moonloader")

-- config
local DELAY = 1
local ACTIVATION_COMBO = { VK_R, VK_CONTROL }
local EXCEPTIONS = {
   -- example:
   -- "sf-integration.lua",
   -- "some-of-your-scripts.luac",

}
--

local function array_includes(array, value)
   for _, element in ipairs(array) do
      if element == value then
         return true
      end
   end
   return false
end

local function wait_for_seconds(seconds)
   local end_time = os.clock() + seconds
   while os.clock() < end_time do
      wait(0)
   end
end

local function match_extension(file)
   local extension = file:match(".+%.+(.+)")
   return extension == "lua" or extension == "luac"
end

local function is_exception(file)
   return array_includes(EXCEPTIONS, file) or file == script.this.filename 
end

local function terminate_all_active_scripts()
   for _, lua_script in ipairs(script.list()) do
      if not is_exception(lua_script.filename) then
         lua_script:unload()
      end
   end
end

local function foreach_file_in_moonloader(callback)
   local path = getWorkingDirectory() .. "\\*"
   local hndl, file = findFirstFile(path)
   for file in findNextFile, hndl do
      if match_extension(file) and not is_exception(file) then
         callback(file)
      end
   end
   findClose(hndl)
end

local function was_no_reload_flag_exported_from_file(file)
   if not is_exception(file) then
      local result, data = pcall(import, file)
      if result and data and data.no_reload then
         return true
      end
   end
   return false
end

local function is_key_combo_pressed(key_combo, call_reason)
   local is_extra_key_down = not key_combo[2] or isKeyDown(key_combo[2])
   local is_main_key_down = call_reason == key_combo[1]
   return is_main_key_down and is_extra_key_down
end

--

local reload_thread = lua_thread.create_suspended(function()
   terminate_all_active_scripts()
   wait(0)
   foreach_file_in_moonloader(script.load)
   wait_for_seconds(DELAY)
end)

local function on_script_load(lua_script)
   local file = lua_script.filename
   if was_no_reload_flag_exported_from_file(file) then
      table.insert(EXCEPTIONS, file)
   end
end

local function on_key_pressed(key)
   local is_thread_suspended = reload_thread:status() ~= "yielded"
   if is_key_combo_pressed(ACTIVATION_COMBO, key) and is_thread_suspended then
      reload_thread:run()
   end
end

--

function onScriptLoad(lua_script)
   on_script_load(lua_script)
end

local windows_message = require("windows.message")
function onWindowMessage(message, wparam)
   if message == windows_message.WM_KEYDOWN or message == windows_message.WM_SYSKEYDOWN then
      on_key_pressed(wparam)
   end
end