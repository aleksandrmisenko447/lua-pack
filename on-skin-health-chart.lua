script_name("on-skin-health-chart")
script_author("THERION")

-- config
local FONT_NAME = "Arial"
local FONT_SIZE = 12
local FONT_FLAGS = 0x5
local FONT_COLOR = 0xFFFFFFFF

local CHART_SIZE_X = 100
local CHART_SIZE_Y = 18
local CHART_BORDER_SIZE = 2

local OFFSET_Z = 0.1
local GAP_BETWEEN_CHARTS = 4

local HEALTH_CHART_FILL = 0xFFF14A4A
local HEALTH_CHART_BORDER_COLOR = 0xFF000000
local HEALTH_CHART_BACKGROUND = 0xFF822828

local CAR_CHART_FILL = 0xFFE5E5E5
local CAR_CHART_BORDER_COLOR = 0xFF000000
local CAR_CHART_BACKGROUND = 0xFF999999
--

local function clamp(value, min, max)
   if value < min then return min end
   if value > max then return max end
   return value
end

local ffi = require("ffi")
local getBonePosition = ffi.cast("int (__thiscall*)(void*, float*, int, bool)", 0x5E4280)

local function isPlayerInFirstPersonCamera()
   return getPlayerInCarCameraMode() == 0
end

local function getCharCenterCoordinates3D(ped)
   if isCharInAnyCar(ped) and not isCharOnAnyBike(ped) then
      local vehicle = storeCarCharIsInNoSave(ped)
      return getCarCoordinates(vehicle)
   end

   local pedPointer = ffi.cast("void*", getCharPointer(ped))
   local vector = ffi.new("float[3]")
   getBonePosition(pedPointer, vector, 2, true)

   return vector[0], vector[1], vector[2]
end

local function drawChart(k, x, y, width, height, borderSize, fillColor, backgroundColor, borderColor)
   renderDrawBoxWithBorder(x, y, width, height, backgroundColor, borderSize, borderColor)
   renderDrawBox(x + borderSize, y + borderSize, k * width - borderSize * 2, height - borderSize * 2, fillColor)
end

local font = nil
local function onInit()
   font = renderCreateFont(FONT_NAME, FONT_SIZE, FONT_FLAGS)
end

local function onEveryFrame()
   if not isCharOnScreen(PLAYER_PED) then
      return
   end

   if isPlayerInFirstPersonCamera() then
      return
   end

   local health = getCharHealth(PLAYER_PED) + getCharArmour(PLAYER_PED)
   local healthPercentage = clamp(health, 0, 100) / 100

   local x, y, z = getCharCenterCoordinates3D(PLAYER_PED)
   z = z + OFFSET_Z
   
   local screenX, screenY = convert3DCoordsToScreen(x, y, z)
   local posX = screenX - CHART_SIZE_X / 2

   if isCharInAnyCar(PLAYER_PED) then
      local vehicle = storeCarCharIsInNoSave(PLAYER_PED)
      local carHealth = getCarHealth(vehicle)
      local carHealthPercentage = clamp(carHealth, 0, 1000) / 1000

      local healthTextPosX = screenX - renderGetFontDrawTextLength(font, health) / 2
      local carTextPosX = screenX - renderGetFontDrawTextLength(font, carHealth) / 2 

      local healthChartPosY = screenY - CHART_SIZE_Y - GAP_BETWEEN_CHARTS / 2
      local healthTextPosY = healthChartPosY + CHART_SIZE_Y / 2 - renderGetFontDrawHeight(font) / 2
      local carChartPosY = healthChartPosY + CHART_SIZE_Y + GAP_BETWEEN_CHARTS
      local carTextPosY = carChartPosY + CHART_SIZE_Y / 2 - renderGetFontDrawHeight(font) / 2

      drawChart(healthPercentage, posX, healthChartPosY, CHART_SIZE_X, CHART_SIZE_Y, CHART_BORDER_SIZE, HEALTH_CHART_FILL, HEALTH_CHART_BACKGROUND, HEALTH_CHART_BORDER_COLOR)
      renderFontDrawText(font, health, healthTextPosX, healthTextPosY, FONT_COLOR)

      drawChart(carHealthPercentage, posX, carChartPosY, CHART_SIZE_X, CHART_SIZE_Y, CHART_BORDER_SIZE, CAR_CHART_FILL, CAR_CHART_BACKGROUND, CAR_CHART_BORDER_COLOR)
      renderFontDrawText(font, carHealth, carTextPosX, carTextPosY, FONT_COLOR)
   else
      local healthTextPosX = screenX - renderGetFontDrawTextLength(font, health) / 2

      local healthChartPosY = screenY - CHART_SIZE_Y / 2
      local healthTextPosY = healthChartPosY + CHART_SIZE_Y / 2 - renderGetFontDrawHeight(font) / 2

      drawChart(healthPercentage, posX, healthChartPosY, CHART_SIZE_X, CHART_SIZE_Y, CHART_BORDER_SIZE, HEALTH_CHART_FILL, HEALTH_CHART_BACKGROUND, HEALTH_CHART_BORDER_COLOR)
      renderFontDrawText(font, health, healthTextPosX, healthTextPosY, FONT_COLOR)
   end
end

function main()
   repeat wait(0) until isSampAvailable()
   onInit()
	while true do
      wait(0)
      onEveryFrame()
	end
end
